#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys


HERE_PATH =  os.path.abspath( os.path.dirname( __file__ ))
sys.path.insert(0, HERE_PATH)

ROOT_PATH = os.path.abspath( os.path.join(HERE_PATH, "..") )
if sys.path.count(ROOT_PATH) == 0:
    sys.path.insert(0, ROOT_PATH)

stream = os.popen('git tag')
output = stream.read()

lst = output.strip().split("\n")
print("------------------ > tags >")
print( sorted(lst))

tag_version = lst[-1]

print("top-version=", tag_version)

with open(os.path.join(ROOT_PATH, "version.txt"), "r") as f:
    f_version = f.read().strip()

print("version.txt=", f_version)

if f_version == tag_version:

    print("OOP! Same new relase")#

    sys.exit(0)


def exec_cmd(cmd):
    print("------------------")
    print("> %s" % cmd)
    stream = os.popen(cmd)
    output = stream.read()
    print(output)

exec_cmd('git commit -a -m "%s" ' % f_version)
exec_cmd('git push origin master ')
exec_cmd('git tag -a %s -m "v%s" ' % (f_version, f_version))
exec_cmd('git push origin %s ' % (f_version))


